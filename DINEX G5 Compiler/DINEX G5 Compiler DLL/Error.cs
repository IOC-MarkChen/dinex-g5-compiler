﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOControls.DINEX.G5
{
    public class Error
    {
        public string Message;
        public int LineNo;
        public int ColNo;
        public string ProgramLine;
    }
}
