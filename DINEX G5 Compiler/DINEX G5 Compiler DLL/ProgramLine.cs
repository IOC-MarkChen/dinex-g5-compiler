﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOControls.DINEX.G5
{
    internal class ProgramLine
    {
        char[] SeparatorList = { ' ', (char)9, ',' }; //Space or TAB

        string _Program = "";
        public string Program
        {
            get { return _Program; }
            set 
            {
                _Program = value;
                Brakedown();
            }
        }

        List<Token> _TokenList = new List<Token>();
        public List<Token> TokenList { get { return _TokenList; } }

        int _LineNo = 0;
        public int LineNo
        {
            get { return _LineNo; }
            set
            {
                _LineNo = value;
                foreach (var aToken in _TokenList) aToken.LineNo = _LineNo;
            }
        }

        UInt32 _Address = 0;
        public UInt32 Address { get { return _Address; } set { _Address = value; } }

        UInt32 _OpCode = 0;
        public UInt32 OpCode { get { return _OpCode; } set { _OpCode = value; } }

        bool _HasError = false;
        public bool HasError { get { return _HasError; } set { _HasError = value; } }

        int _WarningCount = 0;
        public int WarningCount { get { return _WarningCount; } set { _WarningCount = value; } }

        private void Brakedown()
        {
            char ch;
            _TokenList = new List<Token>();
            Token token;
            for (int i = 0; i < _Program.Length; i++)
            {
                ch = _Program[i];
                if (SeparatorList.Contains(ch)) continue;

                if (ch == ';') //Comments
                {
                    token = new Token();
                    token.Name = _Program.Substring(i);
                    token.ColNo = i;
                    token.LineNo = _LineNo;
                    //token.TokenType = TokenTypes.COMMENTS;
                    _TokenList.Add(token);
                    break;
                }
                else
                {
                    for (int j = 0; j + i <= _Program.Length; j++)
                    {
                        if (j + i == _Program.Length) //Last token
                        {
                            token = new Token();
                            token.Name = _Program.Substring(i, j);
                            token.ColNo = i;
                            token.LineNo = _LineNo;
                            //token.TokenType = 
                            _TokenList.Add(token);
                            i += j;
                            break;
                        }

                        if (!SeparatorList.Contains(_Program[j + i]) && _Program[j + i] != ';') continue;
                        else
                        {
                            token = new Token();
                            token.Name = _Program.Substring(i, j);
                            token.ColNo = i;
                            token.LineNo = _LineNo;
                            _TokenList.Add(token);
                            if (_Program[j + i] == ';')
                                i += j - 1;
                            else
                                i += j;
                            break;
                        }
                    }
                }
            }
        } //End of Brakedown
    }
}
