﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace IOControls.DINEX.G5
{
    [Serializable]
    public class SynTreeNode
    {
        public string NodeName;
        public UInt32 OPCode = 0;
        public int Number;
        public TokenTypes TokenType;
        public List<SynTreeNode> Nodes;
        public List<int> NodIDs = new List<int>();
        public int ID = 0;
        public int Length = 0;
        public int Position = 0;
        public string Description = "";


        public SynTreeNode(): this("")
        {
            
        }

        public SynTreeNode(string name)
        {
            NodeName = name;
            OPCode = 0x0;
            Number = -1;
            TokenType = TokenTypes.END;
            Nodes = new List<SynTreeNode>();
        }

        public SynTreeNode CopyData(bool withID)
        {
            SynTreeNode sNode = new SynTreeNode();
            if (withID) sNode.ID = this.ID;
            sNode.Length = this.Length;
            sNode.NodeName = this.NodeName;
            sNode.Number = this.Number;
            sNode.OPCode = this.OPCode;
            sNode.Position = this.Position;
            sNode.TokenType = this.TokenType;
            sNode.Description = this.Description;
            return sNode;
        }

        public SynTreeNode CopyData()
        {
            return CopyData(true);
        }
    }



    [Serializable]
    public class SyntaxNodeList
    {
        static public SynTreeNode TopNode = null;
        static public List<SynTreeNode> Items = new List<SynTreeNode>();
            
        static public void Save(string filePath)
        {
            string xmlString;
            xmlString = SerializerHelper.BuildXmlByObject<List<SynTreeNode>>(Items);
            File.WriteAllText(filePath, xmlString);
        }

        static public void Load(string filePath)
        {
            string xmlString;
            xmlString = File.ReadAllText(filePath);
            SyntaxNodeList sTree;
            Items = SerializerHelper.BuildObjectByXmlString<List<SynTreeNode>>(xmlString);
            TopNode = Items[0];
        }

        static public  SynTreeNode Find(int ID)
        {
            foreach (var aNode in Items)
                if (aNode.ID == ID) return aNode;
            return null;
        }
    }
}
