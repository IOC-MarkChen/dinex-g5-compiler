﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace IOControls.DINEX.G5
{

    public class SerializerHelper
    {
        /// <summary>
        /// 对象到XML-----泛类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string BuildXmlByObject<T>(T obj)
        {
            if (obj == null) return null;
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            MemoryStream stream = new MemoryStream();
            XmlTextWriter xtw = new XmlTextWriter(stream, Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            //try
            //{
            serializer.Serialize(stream, obj, ns);
            //}
            //catch (Exception ex)
            //{
            //    return null;
            //}
            stream.Position = 0;
            StringBuilder xmlString = new StringBuilder();
            using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
            {
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    xmlString.AppendLine(line);
                }
            }
            return xmlString.ToString();
        }

        /// <summary>
        /// XML到反序列化到对象----支持泛类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static T BuildObjectByXmlString<T>(string xmlString)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(stream, Encoding.UTF8))
                {
                    sw.Write(xmlString);
                    sw.Flush();
                    stream.Seek(0, SeekOrigin.Begin);
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    try
                    {
                        return ((T)serializer.Deserialize(stream));
                    }
                    catch (Exception ex)
                    {
                        //TODO:写日志
                        throw new Exception("Invalid Xml file format: " + ex.Message);
                    }
                }
            }
        }
    }
}
