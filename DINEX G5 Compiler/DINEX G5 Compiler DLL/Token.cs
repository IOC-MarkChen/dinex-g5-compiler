﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOControls.DINEX.G5
{
    public class Token
    {
        #region --Static Member

        static private Dictionary<TokenTypes, int> KeyWordLength = new Dictionary<TokenTypes, int>();
        static Token()
        {
            //The reason for this key word length is for LABEL define.
            //  A label "AND" is allowed because its length is not 4
            //  Key words that are not defined here use the same length as the key word it self, such as "CLEAR". It is not allowed to be a label
            //  The length is less than the key word itself means it is allowed.

            KeyWordLength.Add(TokenTypes.ADD, 4);
            KeyWordLength.Add(TokenTypes.AND, 4);
            KeyWordLength.Add(TokenTypes.B, 4);
            KeyWordLength.Add(TokenTypes.COMMENTS, 0);
            KeyWordLength.Add(TokenTypes.CONST, 0);
            KeyWordLength.Add(TokenTypes.DIV, 4);
            KeyWordLength.Add(TokenTypes.DO_, 4);
            KeyWordLength.Add(TokenTypes.DO_COUNT, 9);
            KeyWordLength.Add(TokenTypes.EQUAL_TO, 0);
            KeyWordLength.Add(TokenTypes.GT_THAN, 0);
            KeyWordLength.Add(TokenTypes.ID, 4);
            KeyWordLength.Add(TokenTypes.IN_IOA, 7);
            KeyWordLength.Add(TokenTypes.IN_IOB, 7);
            KeyWordLength.Add(TokenTypes.IN_IOC, 7);
            KeyWordLength.Add(TokenTypes.IN_IOD, 7);
            KeyWordLength.Add(TokenTypes.LABEL, 0); 
            KeyWordLength.Add(TokenTypes.LABEL_REF, 0); 
            KeyWordLength.Add(TokenTypes.LESS_THAN, 0);
            KeyWordLength.Add(TokenTypes.LOOP_, 6);
            KeyWordLength.Add(TokenTypes.MB_INPUT, 9);
            KeyWordLength.Add(TokenTypes.MB_OUTPUT, 10);
            KeyWordLength.Add(TokenTypes.MUL, 4);
            KeyWordLength.Add(TokenTypes.NOTEQUAL_TO, 0);
            KeyWordLength.Add(TokenTypes.OR, 3);
            KeyWordLength.Add(TokenTypes.OUT_IOA, 8);
            KeyWordLength.Add(TokenTypes.OUT_IOB, 8);
            KeyWordLength.Add(TokenTypes.OUT_IOC, 8);
            KeyWordLength.Add(TokenTypes.OUT_IOD, 8);
            KeyWordLength.Add(TokenTypes.R, 2);
            KeyWordLength.Add(TokenTypes.SUB, 4);
            KeyWordLength.Add(TokenTypes.TMR, 5);
            KeyWordLength.Add(TokenTypes.V, 3);
            KeyWordLength.Add(TokenTypes.VALUE, 0); 
            KeyWordLength.Add(TokenTypes.XOR, 4);
        }

        #endregion --Static Member

        #region --Private Member
        //The private members with a public properties are not defined here
        #endregion --Private Member

        #region --Public Propertied
        int _LineNo;
        public int LineNo { get { return _LineNo; } set { _LineNo = value; } }

        int _ColNo;
        public int ColNo { get { return _ColNo; } set { _ColNo = value; } }

        string _Name;
        public string Name { get { return _Name; } set { _Name = value.ToUpper(); TokenParse(); } }

        UInt32 _Number = 0xFFFFFFFF;
        public UInt32 Number { get { return _Number; } }
        public void SetNumber(UInt32 num) { _Number = num; }

        TokenTypes _TokenType;
        public TokenTypes TokenType { get { return _TokenType; } set { _TokenType = value; } }

        string _ErrorMsg = "";
        public string ErrorMsg { get { return _ErrorMsg; } set { _ErrorMsg = value; } }

        #endregion --Public Propertied

        #region --Constructors
        #endregion --Constructors

        #region --Public Methods
        #endregion --Public Methods

        #region --Privae Methods
        private void TokenParse()
        {
            _TokenType = TokenTypes.UNKNOWN;

            UInt32 tmpUint32 = 0xffffffff;

            if (_Name == "AND")
            { }

            #region --VALUE
            if (_Name[0] >= '0' && _Name[0] <= '9')
            {
                _TokenType = TokenTypes.VALUE;
                tmpUint32 = ConvertNumber(_Name);
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 >= 0x20000) ErrorMsg = "Invalid number"; 
                _Number = tmpUint32;
                return;
            }
            #endregion --VALUE


            //Check if it is exact the same
            if (Enum.TryParse<TokenTypes>(_Name, out _TokenType))
            {
                int len = 0;
                if (KeyWordLength.TryGetValue(_TokenType, out len))
                {
                    if (_Name.Length == KeyWordLength[_TokenType])
                        return;
                    else
                        _TokenType = TokenTypes.UNKNOWN;
                }
                else return;
            }

            //Check Special cases
            #region --B, R, V, TMR, MB_
            if (_Name[0] == ';')
                _TokenType = TokenTypes.COMMENTS;
            else if (_Name[0] == 'R' && _Name.Length == 2 &&
                (_Name[1] == 'X' || _Name[1] == 'Y' || _Name[1] == 'Z'))
            {
                switch (_Name[1])
                {
                    case 'X': _Number = 0; break;
                    case 'Y': _Number = 1; break;
                    case 'Z': _Number = 2; break;
                }
                _TokenType = TokenTypes.R;
            }
            else if (_Name[0] == 'B' && _Name.Length >2 && _Name.Length <= 4 && _Name[1] == '.')
            {
                _TokenType = TokenTypes.B;
                tmpUint32 = ConvertNumber(_Name.Substring(2));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 15)
                {
                    ErrorMsg = "B.0 - B.15" + " expected";
                }
                _Number = tmpUint32;
            }
            else if (_Name[0] == 'V' && _Name.Length == 3)
            {
                _TokenType = TokenTypes.V;
                tmpUint32 = ConvertNumber("0x" + _Name.Substring(1));
                if (tmpUint32 == 0xFFFFFFFF)
                {
                    ErrorMsg = "V00-VFF" + " expected";
                }
                _Number = tmpUint32;
            }
            else if (_Name.Length == 5 && _Name.Substring(0, 3) == "TMR")
            {
                _TokenType = TokenTypes.TMR;
                tmpUint32 = ConvertNumber("0x" + _Name.Substring(3));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 0x7F)
                {
                    ErrorMsg = "TMR00-TMR7F" + " expected";
                }
                _Number = tmpUint32;
            }
            else if (_Name.Length == 9 && _Name.Substring(0, 8) == "MB_INPUT")
            {
                _TokenType = TokenTypes.MB_INPUT;
                tmpUint32 = ConvertNumber(_Name.Substring(8));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 3)
                {
                    ErrorMsg = "MB_INPUT0 - 3" + " expected";
                }
                _Number = tmpUint32;
            }
            else if (_Name.Length == 10 && _Name.Substring(0, 9) == "MB_OUTPUT")
            {
                _TokenType = TokenTypes.MB_OUTPUT;
                tmpUint32 = ConvertNumber(_Name.Substring(9));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 3)
                {
                    ErrorMsg = "MB_OUTPUT0 - 3" + " expected";
                }
                _Number = tmpUint32;
            }
            #endregion --Length == 1: B, R, V, TMR

            #region -- =, <, >, ==, !=
            else if (_Name == "=")
                _TokenType = TokenTypes.ASSIGN;
            else if (_Name == "<")
                _TokenType = TokenTypes.LESS_THAN;
            else if (_Name == ">")
                _TokenType = TokenTypes.GT_THAN;
            else if (_Name == "==")
                _TokenType = TokenTypes.EQUAL_TO;
            else if (_Name == "!=")
                _TokenType = TokenTypes.NOTEQUAL_TO;
            #endregion -- =, <, >, ==, !=

            #region --IN_, OUT_
            else if (_Name.Length == 7 && _Name.Substring(0, 6) == "IN_IOA")
            {
                _TokenType = TokenTypes.IN_IOA;
                tmpUint32 = ConvertNumber(_Name.Substring(6, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "IN_IOA0-7" + " expected";
                _Number = tmpUint32;
            }
            else if (_Name.Length == 7 && _Name.Substring(0, 6) == "IN_IOB")
            {
                _TokenType = TokenTypes.IN_IOB;

                tmpUint32 = ConvertNumber(_Name.Substring(6, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "IN_IOB0-7" + " expected";
                _Number = tmpUint32;
            }
            else if (_Name.Length == 7 && _Name.Substring(0, 6) == "IN_IOC")
            {
                _TokenType = TokenTypes.IN_IOC;

                tmpUint32 = ConvertNumber(_Name.Substring(6, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "IN_IOC0-7" + " expected";
                _Number = tmpUint32;
            }
            else if (_Name.Length == 7 && _Name.Substring(0, 6) == "IN_IOD")
            {
                _TokenType = TokenTypes.IN_IOD;

                tmpUint32 = ConvertNumber(_Name.Substring(6, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "IN_IOD0-7" + " expected";
                _Number = tmpUint32;
            }
            else if (_Name.Length == 8 && _Name.Substring(0, 7) == "OUT_IOA")
            {
                _TokenType = TokenTypes.OUT_IOA;

                tmpUint32 = ConvertNumber(_Name.Substring(7, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "OUT_IOA0-7" + " expected";
                _Number = tmpUint32;
            }
            else if (_Name.Length == 8 && _Name.Substring(0, 7) == "OUT_IOB")
            {
                _TokenType = TokenTypes.OUT_IOB;

                tmpUint32 = ConvertNumber(_Name.Substring(7, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "OUT_IOB0-7" + " expected";
                _Number = tmpUint32;
            }
            else if (_Name.Length == 8 && _Name.Substring(0, 7) == "OUT_IOC")
            {
                _TokenType = TokenTypes.OUT_IOC;

                tmpUint32 = ConvertNumber(_Name.Substring(7, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "OUT_IOC0-7" + " expected";
                _Number = tmpUint32;
            }
            else if (_Name.Length == 8 && _Name.Substring(0, 7) == "OUT_IOD")
            {
                _TokenType = TokenTypes.OUT_IOD;

                tmpUint32 = ConvertNumber(_Name.Substring(7, 1));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 7) ErrorMsg = "OUT_IOD0-7" + " expected";
                _Number = tmpUint32;
            }
            #endregion --IN_, OUT_

            #region --DO_, LOOP_, DO_COUNT
            else if (_Name.Length == 9 && _Name.Substring(0, 8) == "DO_COUNT")
            {
                _TokenType = TokenTypes.DO_COUNT;
                switch (_Name[8])
                {
                    case 'A': _Number = 0; break;
                    case 'B': _Number = 1; break;
                    case 'C': _Number = 2; break;
                    case 'D': _Number = 3; break;
                    default:
                        ErrorMsg = "DO_COUNTA - DO_COUNTD" + " expected";
                        break;
                }

            }
            else if (_Name.Length == 4 && _Name.Substring(0, 3) == "DO_")
            {
                _TokenType = TokenTypes.DO_;
                switch (_Name[3])
                {
                    case 'A': _Number = 0; break;
                    case 'B': _Number = 2; break;
                    case 'C': _Number = 4; break;
                    case 'D': _Number = 6; break;
                    default:
                        ErrorMsg = "DO_A - DO_D" + " expected";
                        break;
                }

            }
            else if (_Name.Length == 6 && _Name.Substring(0, 5) == "LOOP_")
            {
                _TokenType = TokenTypes.LOOP_;
                switch (_Name[5])
                {
                    case 'A': _Number = 0; break;
                    case 'B': _Number = 2; break;
                    case 'C': _Number = 4; break;
                    case 'D': _Number = 6; break;
                    default:
                        ErrorMsg = "LOOP_A - LOOP_D" + " expected";
                        break;
                }

            }
            #endregion --DO_, LOOP_, DO_COUNT

            #region --ID
            else if (_Name.Length == 4 && _Name.Substring(0, 2) == "ID")
            {
                _TokenType = TokenTypes.ID;

                tmpUint32 = ConvertNumber(_Name.Substring(2, 2));
                if (tmpUint32 == 0xFFFFFFFF || tmpUint32 > 95 || tmpUint32 < 64) ErrorMsg = "ID64 - 95" + " expected";
                _Number = tmpUint32 - 64;
            }

            #endregion --ID

            #region --AND, OR, XOR, ADD, SUB, MUL, DIV
            else if (_Name.Length == 4 && (_Name.Substring(0, 3) == "AND" || _Name.Substring(0, 3) == "XOR" || _Name.Substring(0, 3) == "ADD" ||
                _Name.Substring(0, 3) == "SUB" || _Name.Substring(0, 3) == "MUL" || _Name.Substring(0, 3) == "DIV"))
            {
                _TokenType = (TokenTypes)Enum.Parse(typeof(TokenTypes), _Name.Substring(0, 3));

                if (_Name[3] == 'W') _Number = 0;
                else if (_Name[3] == 'R') _Number = 1;
                else ErrorMsg = "Syntax Error";
            }
            else if (_Name.Length == 3 && _Name.Substring(0, 2) == "OR")
            {
                _TokenType = (TokenTypes)Enum.Parse(typeof(TokenTypes), _Name.Substring(0, 2));

                if (_Name[2] == 'W') _Number = 0;
                else if (_Name[2] == 'R') _Number = 1;
                else ErrorMsg = "Syntax Error";
            }
            #endregion --AND, OR, XOR, ADD, SUB, MUL, DIV

            else
            {
                _TokenType = TokenTypes.UNKNOWN;
                ErrorMsg = "Syntax Error";
            }
        }

        private UInt32 ConvertNumber(string strNumber)
        {
            UInt32 tmp = 0xFFFFFFFF;
            if (strNumber.Length > 2 && strNumber.Substring(0, 2).ToUpper() == "0X") //Hex
            {
                if (UInt32.TryParse(strNumber.Substring(2), System.Globalization.NumberStyles.HexNumber, null, out tmp))
                    return tmp;
                else return 0xFFFFFFFF;
            }
            else
            {
                if (UInt32.TryParse(strNumber, out tmp))
                    return tmp;
                else return 0xFFFFFFFF;
            }
            //return tmp;
        }

        #endregion --Privae Methods


    }

    public enum TokenTypes
    {
        UNKNOWN,
        ADD,
        AND,
        ASSIGN,
        B,
        CALL,
        CLEAR,
        COMMENTS,
        CONST,
        DECF,
        DIV,
        DIVW_M,
        DO_,
        DO_COUNT,
        END,
        EQU,
        EQUAL_TO,
        GOTO,
        GT_THAN,
        ID,
        IF,
        IN_IOA,
        IN_IOB,
        IN_IOC,
        IN_IOD,
        INCF,
        OUT_IOA,
        OUT_IOB,
        OUT_IOC,
        OUT_IOD,
        MB_INPUT,
        MB_OUTPUT,
        MUL,
        NOP,
        NOTEQUAL_TO,
        LABEL,
        LABEL_REF,
        LESS_THAN,
        LET,
        LOOP_,
        OR,
        R,
        READ,
        RETURN,
        RLF,
        RRF,
        SET,
        SKIPIF,
        STATUS,
        SUB,
        SWAP,
        TMR,
        V,
        VALUE,
        VALUE1BIT,
        VALUE4BIT, 
        VALUE16BIT,
        VALUE17BIT,
        W,
        W_M,
        W_R,
        WRITE,
        XOR
    }
}
