﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOControls.DINEX.G5
{
    public class ProgramLabel
    {
        public string Name = "";
        public UInt32 Value = 0;
        public string ValueStr = "";
    }
}
