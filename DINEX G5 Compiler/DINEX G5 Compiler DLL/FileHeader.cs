﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOControls.DINEX.G5
{
    public class FileHeader
    {
        public string TargetSystem = "G5MBC";
        public UInt32 TotalBytes = 0;
        public UInt16 ChecksumFile = 0;
        public DateTime Time = DateTime.Now;
        
        public byte Version1 = 0;
        public byte Version2 = 0;
        public byte Version3 = 0;
        public List<CanID> CanIDList = new List<CanID>();
        public List<byte> TimerNoList = new List<byte>();
        public UInt16 ChecksumInstruction = 0;
        public UInt32 InstructionCount = 0;

    }
}
