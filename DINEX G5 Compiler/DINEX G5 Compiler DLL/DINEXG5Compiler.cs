﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace IOControls.DINEX.G5
{
    public partial class DINEXG5Compiler
    {
        public List<UInt32> IDList = new List<UInt32>();
        public List<UInt32> TMRList = new List<UInt32>();
        public List<ExecTime> ExecTimeList = new List<ExecTime>();
        public byte[] Version = new byte[10];

        public  List<ProgramLabel> LabelList = new List<ProgramLabel>();
        public List<Error> ErrorList = new List<Error>();

        public UInt16 ChecksumObjFile = 0;
        public UInt16 ChecksumInstructions = 0;

        public UInt32 TotalAddr = 0;
        
        private UInt32 CurrentAddr = 0;
        List<ProgramLine> ProgramLineList;        
        

        public bool Compile(string filePath)
        {
            bool pass = true;
            string[] lines = new string[0];
            ProgramLine pLine;
            ExecTime exTime = new ExecTime();
            exTime.Name = "Start Compile";
            ExecTimeList.Add(exTime);

            #region --0.0 Load Syntax Tree Node
            if (SyntaxNodeList.Items.Count == 0)
            {
                string fileName = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                fileName += "\\SyntaxNodeList1.xml";
                SyntaxNodeList.Load(fileName);
            }

            #endregion --0.0 Load Syntax Tree Node

            exTime = new ExecTime();
            exTime.Name = "Load Syntax Tree";
            ExecTimeList.Add(exTime);

            #region --1.0 Read the file
            ProgramLineList = new List<ProgramLine>();
            try
            {
                lines = File.ReadAllLines(filePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            #endregion --1.0 Read the file

            exTime = new ExecTime();
            exTime.Name = "Read the SRC file";
            ExecTimeList.Add(exTime);

            #region --2.0 Breakdown into token list
            for (int i = 0; i < lines.Length; i++)
            {
                pLine = new ProgramLine();
                pLine.LineNo = i + 1;
                pLine.Program = lines[i];
                ProgramLineList.Add(pLine);
            }

            exTime = new ExecTime();
            exTime.Name = "Breakdown into token list";
            ExecTimeList.Add(exTime);

            //2.1 Generate label list
            ProgramLabel label;
            foreach (var aLine in ProgramLineList)
            {
                if (aLine.TokenList.Count > 0 && aLine.TokenList[0].TokenType == TokenTypes.UNKNOWN)
                {
                    //Define constant
                    if (aLine.TokenList.Count >= 3 && aLine.TokenList[1].TokenType == TokenTypes.EQU)
                    {
                        aLine.TokenList[0].TokenType = TokenTypes.LABEL;
                        aLine.TokenList[0].ErrorMsg = "";
                        if (CheckDuplicateLabel(aLine.TokenList[0].Name))
                        {
                            aLine.HasError = true;
                            aLine.TokenList[0].ErrorMsg = "Duplicate Label";
                            AddError(aLine.TokenList[0]);
                        }
                        else
                        {
                            //Change the const token type
                            aLine.TokenList[2].TokenType = TokenTypes.CONST;
                            //Add to label list
                            label = new ProgramLabel();
                            label.Name = aLine.TokenList[0].Name;
                            label.Value = aLine.TokenList[2].Number;
                            label.ValueStr = aLine.TokenList[2].Name;
                            LabelList.Add(label);
                        }
                    }

                    //Define Address Label
                    else if ((aLine.TokenList.Count == 1) || (aLine.TokenList.Count == 2 && aLine.TokenList[1].TokenType == TokenTypes.COMMENTS))
                    {
                        if (aLine.TokenList[0].ColNo == 0)
                        {
                            aLine.TokenList[0].TokenType = TokenTypes.LABEL;
                            aLine.TokenList[0].ErrorMsg = "";
                            if (CheckDuplicateLabel(aLine.TokenList[0].Name))
                            {
                                aLine.HasError = true;
                                aLine.TokenList[0].ErrorMsg = "Duplicate Label";
                                AddError(aLine.TokenList[0]);
                            }
                            else
                            {
                                label = new ProgramLabel();
                                label.Name = aLine.TokenList[0].Name;
                                label.Value = aLine.Address;
                                LabelList.Add(label);
                            }
                        }
                    }
                }
            }

            exTime = new ExecTime();
            exTime.Name = "Generate label list";
            ExecTimeList.Add(exTime);

            #endregion --2.0 Breakdown into token list

            #region --3.0 Parsing
            Parsing(1);
            exTime = new ExecTime();
            exTime.Name = "Parse phase 1";
            ExecTimeList.Add(exTime);

            TotalAddr = CurrentAddr;           
            Parsing(2);
            exTime = new ExecTime();
            exTime.Name = "Parse phase 2";
            ExecTimeList.Add(exTime);

            #endregion --3.0 Parsing


            #region --4.0 Generate LST file
            string lstFileName = Path.GetDirectoryName(filePath);
            lstFileName += "\\" + Path.GetFileNameWithoutExtension(filePath);
            lstFileName += ".LST";
            List<string> lstLines = new List<string>();
            string lstLine;
            lstLine = "Line #: Address   OP_Code           -Program Lsiting";
            lstLines.Add(lstLine);
            foreach (var aLine in ProgramLineList)
            {
                lstLine = aLine.LineNo.ToString("000000") + ": 0x" + aLine.Address.ToString("X5") + "   ";
                if (aLine.OpCode != 0xffffffff)
                    lstLine += "0x" + aLine.OpCode.ToString("X8");
                else lstLine += "          ";
                lstLine += "        " + aLine.Program;
                lstLines.Add(lstLine);

                //Error Message
                if (aLine.HasError || aLine.WarningCount > 0)
                {
                    foreach (var aToken in aLine.TokenList)
                    {
                        if (!string.IsNullOrEmpty(aToken.ErrorMsg))
                        {
                            aToken.ErrorMsg = "^" + aToken.ErrorMsg;
                            int tmpInt = aToken.ColNo + aToken.ErrorMsg.Length + 31;
                            lstLine = "Error:" + aToken.ErrorMsg.PadLeft(tmpInt);
                            lstLines.Add(lstLine);
                            break;
                        }
                    }
                }
            }

            File.WriteAllLines(lstFileName, lstLines);

            #endregion --4.0 Generate LST file

            exTime = new ExecTime();
            exTime.Name = "Generate LST file";
            ExecTimeList.Add(exTime);


            #region --5.0 Generate OBJ file
            byte[] byteData;
            ASCIIEncoding ascii = new ASCIIEncoding();
            string objFileName = Path.GetDirectoryName(filePath);
            objFileName += "\\" + Path.GetFileNameWithoutExtension(filePath);
            objFileName += ".OBJ";
            if (File.Exists(objFileName)) File.Delete(objFileName);
            if (ErrorList.Count == 0)
            {
                List<byte> objList = new List<byte>();

                #region --5.1 Generate File Header


                    string[] systemLineSplite = ProgramLineList[3].Program.Split(' ');
                    if (systemLineSplite[5] == "G6") byteData = ascii.GetBytes("G6MBC");
                    else byteData = ascii.GetBytes("G5MBC");
                
                
                objList.AddRange(byteData);

                //Total Bytes
                byteData = BitConverter.GetBytes((uint)0xffffffff);
                objList.AddRange(byteData);

                //Checksum 
                byteData = BitConverter.GetBytes((UInt16)0xffff);
                objList.AddRange(byteData);

                //DateTime 
                byteData = AddDateTimeToFile(DateTime.Now);
                objList.AddRange(byteData);

                //File Version
                for (int i = 0; i < 10; i++)
                    if (Version.Length > i)
                        objList.Add(Version[i]);
                    else objList.Add(0xFF);

                //Reserved
                for (int i = 0; i < 101; i++) objList.Add(0xFF);

                //Module CAN ID
                objList.Add((byte)IDList.Count);
                for (int i = 0; i < 32; i++)
                {
                    if (i < IDList.Count)
                        objList.Add((byte)IDList[i]);
                    else
                        objList.Add(0xff);
                    objList.Add(0xff);
                }

                //Timer Area
                objList.Add((byte)TMRList.Count);
                for (int i = 0; i < 128; i++)
                {
                    if (i < TMRList.Count)
                        objList.Add((byte)TMRList[i]);
                    else
                        objList.Add(0xff);
                    objList.Add(0xff);
                }

                //Reserved Area
                for (int i = 0; i < 568; i++) objList.Add(0xff);

                //Instriction checksum
                objList.Add(0xff);
                objList.Add(0xff);

                //Total Instructions package count
                byteData = BitConverter.GetBytes(CurrentAddr);
                objList.AddRange(byteData);

                int count = objList.Count;

                #endregion --5.1 Generate File Header


                byte[] tmpCode;
                byte tByte;
                uint programAddress = 0xFFFFFFFF;
                foreach (var aLine in ProgramLineList)
                {
                    if (aLine.OpCode != 0xffffffff)
                    {
                        tmpCode = BitConverter.GetBytes(aLine.OpCode);
                        if (aLine.Address != programAddress)
                        {
                            objList.AddRange(tmpCode);
                            programAddress = aLine.Address;
                        }
                        else
                            for (int i = 0; i < 4; i++)
                                objList[0x400 + (int)programAddress * 4 + i] = tmpCode[i];
                    }
                }

                //////Fill up 0xFF to 512K
                ////for (uint i = CurrentAddr * 4; i < 0x80000; i++) objList.Add(0xff);

                //Fill the total length
                byteData = BitConverter.GetBytes((UInt32)(objList.Count - 11));
                for (int i = 0; i < 4; i++)
                    objList[i + 5] = byteData[i];

                //Cauculate instruction checksum first
                UInt16 chkSum = 0;
                int tmpInt = 1024 + (int)CurrentAddr * 4;
                for (int i = 1024; i < tmpInt; i++) chkSum += objList[i];
                this.ChecksumInstructions = chkSum;

                byteData = BitConverter.GetBytes(chkSum);
                objList[1018] = byteData[0];
                objList[1019] = byteData[1];

                //Cauculate whole file checksum 
                chkSum = 0;
                ////for (int i = 11; i < 0x80400; i++) chkSum += objList[i];
                for (int i = 11; i < objList.Count; i++) chkSum += objList[i];

                this.ChecksumObjFile = chkSum;
                byteData = BitConverter.GetBytes(chkSum);
                objList[9] = byteData[0];
                objList[10] = byteData[1];

                File.WriteAllBytes(objFileName, objList.ToArray());
            }
            else
                pass = false;
            #endregion --5.0 Generate OBJ file

            exTime = new ExecTime();
            exTime.Name = "Generate OBJ file";
            ExecTimeList.Add(exTime);


            #region --6.0 Generate LOG file
            
            string logFileName = Path.GetDirectoryName(filePath);
            logFileName += "\\" + Path.GetFileNameWithoutExtension(filePath);
            logFileName += ".LOG";
            List<string> logLines = new List<string>();
            string line;

            if (pass)
            {
                line = "Successfully Compiled in " + DateTime.Now.ToString();
                logLines.Add(line);

                line = "OBJ File Checksum: " + ChecksumObjFile.ToString("X4");
                logLines.Add(line);
                line = "Instruction Checksum: " + ChecksumInstructions.ToString("X4");
                logLines.Add(line);
                logLines.Add("");

                ////logLines.Add("=========================");
                ////line = "Total Label Defined: " + LabelList.Count.ToString();
                ////logLines.Add(line);
                ////logLines.Add("");

                ////for (int i = 0; i < LabelList.Count; i++)
                ////{
                ////    line = LabelList[i].Name + ": 0x" + LabelList[i].Value.ToString("X8");
                ////    logLines.Add(line);
                ////}

                logLines.Add("=========================");
                //logLines.Add("");


                logLines.Add("Total Module ID: " + IDList.Count.ToString());
                line = "";
                foreach (var aItem in IDList)
                    line += ((aItem + 64).ToString("00") + ", ");
                logLines.Add(line);
                logLines.Add("=========================");
                //logLines.Add("");

                ////logLines.Add("Total Timer: " + TMRList.Count.ToString());
                ////line = "";
                ////foreach (var aItem in TMRList)
                ////    line += (aItem.ToString("X2") + ", ");
                ////logLines.Add(line);
                ////logLines.Add("=========================");
            }
            else
            {
                logLines.Add("Total Error: " + ErrorList.Count.ToString());
                logLines.Add("");
                foreach (Error aError in ErrorList)
                {
                    logLines.Add("Line No: " + aError.LineNo.ToString("00000") + ": " + aError.ProgramLine + " (" + aError.Message + ")");
                }
            }
            File.WriteAllLines(logFileName, logLines);

            #endregion --6.0 Generate LOG file

            exTime = new ExecTime();
            exTime.Name = "Generate LOG file";
            ExecTimeList.Add(exTime);

            exTime = new ExecTime();
            exTime.Name = "End Compile";
            ExecTimeList.Add(exTime);

            return pass;
        }

        private byte[] AddDateTimeToFile(DateTime date)
        {
            byte[] data = new byte[6];

            int iTemp = 0;
            iTemp = date.Month;
            iTemp = Convert.ToInt16(iTemp.ToString(), 16);
            data[0] = (byte)iTemp;

            iTemp = date.Day;
            iTemp = Convert.ToInt16(iTemp.ToString(), 16);
            data[1] = (byte)iTemp;

            iTemp = date.Year;
            iTemp -= 2000;
            iTemp = Convert.ToInt16(iTemp.ToString(), 16);
            data[2] = (byte)iTemp;

            iTemp = date.Hour;
            iTemp = Convert.ToInt16(iTemp.ToString(), 16);
            data[3] = (byte)iTemp;

            iTemp = date.Minute;
            iTemp = Convert.ToInt16(iTemp.ToString(), 16);
            data[4] = (byte)iTemp;

            iTemp = date.Second;
            iTemp = Convert.ToInt16(iTemp.ToString(), 16);
            data[5] = (byte)iTemp;

            return data;           
        }

        private void Parsing(int phaseNo)
        {
            UInt32 tmpOpCode;
            SynTreeNode currentNode = null, nextNode = null;
            bool found;
            bool complete;
            CurrentAddr = 0;
            List<string> DoLoopStack = new List<string>();
            Token lastToken = null;

            ProgramLine aLine;
            for(int i = 0; i < ProgramLineList.Count; i++)
            {
                aLine = ProgramLineList[i];
                if (i == 415)
                { }

                #region --Handle Program Address

                #region --1.0 Check if it is non instruction line
                if (aLine.TokenList.Count == 0 || aLine.TokenList[0].TokenType == TokenTypes.COMMENTS)
                {
                    aLine.Address = CurrentAddr;
                    aLine.OpCode = 0xffffffff;

                    continue;
                }
                #endregion --1.0 Check if it is non instruction line

                #region --2.0 Check if it is address LABEL line
                else if (aLine.TokenList[0].TokenType == TokenTypes.LABEL)
                {
                    aLine.Address = CurrentAddr;
                    aLine.OpCode = 0xffffffff;

                    if (phaseNo == 2 && !string.IsNullOrEmpty(aLine.TokenList[0].ErrorMsg))
                    {
                        if (!aLine.HasError)
                        {
                            aLine.HasError = true;
                            AddError(aLine.TokenList[0]);
                        }
                    }
                    if (aLine.TokenList.Count < 3) //Address Label
                    {
                        foreach (var aLabel in LabelList)
                            if (aLabel.Name == aLine.TokenList[0].Name)
                                aLabel.Value = aLine.Address;
                    }
                    else //Const label
                    {
                        aLine.OpCode = 0;
                    }

                }
                #endregion --2.0 Check if it is address LABEL line

                #region --3.0 Instruction line
                else
                {
                    aLine.Address = CurrentAddr;
                    CurrentAddr++;
                    if (CurrentAddr > 0x20000)
                    {
                        if (!aLine.HasError)
                        {
                            aLine.HasError = true;
                            aLine.TokenList[0].ErrorMsg = "Address overflow";
                            AddError(aLine.TokenList[0]);
                            return;
                        }
                    }
                }
                #endregion --3.0 Instruction line
                #endregion --Handle Program Address

                #region --Handle OP Code
                if (phaseNo == 2)
                {
                    currentNode = SyntaxNodeList.TopNode;
                    complete = false;
                    Token aToken;
                    for (int j = 0; j < aLine.TokenList.Count; j++)
                    {
                        aToken = aLine.TokenList[j];
                        lastToken = aToken;

                        #region --3.0.1 Convert Label in the instruction phase 2 only
                        if (phaseNo == 2 && aToken.TokenType == TokenTypes.UNKNOWN)
                        {
                            foreach (var aLebel in LabelList)
                            {
                                if (aLebel.Name == aToken.Name)
                                {
                                    if (string.IsNullOrEmpty(aLebel.ValueStr))
                                    {
                                        aToken.TokenType = TokenTypes.LABEL_REF;
                                        aToken.ErrorMsg = ""; //Clear error in phase 1
                                        aToken.SetNumber(aLebel.Value);
                                    }
                                    else
                                    {
                                        aToken.ErrorMsg = ""; //Clear error in phase 1
                                        aToken.Name = aLebel.ValueStr;
                                    }
                                }
                            }
                        }
                        #endregion --3.0.1 Check Label in the in--3.0.1 Convert Label in the instruction phase 2 only

                        #region --3.1 Check Syntax error
                        if (phaseNo == 2 && !string.IsNullOrEmpty(aToken.ErrorMsg))
                        {
                            if (!aLine.HasError)
                            {
                                aLine.HasError = true;
                                AddError(aToken);
                            }
                        }
                        #endregion --3.1 Check Syntax error

                        #region --3.1.1 Generate Module ID and TMR lists for header file
                        if (aToken.TokenType == TokenTypes.ID) AddID(aToken.Number);
                        if (aToken.TokenType == TokenTypes.TMR) AddTMR(aToken.Number);
                        #endregion --3.1.1 Generate Module ID and TMR lists for header file

                        #region --3.2 Check if it is valid
                        if (currentNode.NodIDs.Count != 0)
                        {
                            found = false;
                            foreach (int aID in currentNode.NodIDs)
                            {
                                SynTreeNode aNode = SyntaxNodeList.Find(aID);

                                #region --3.2.1 Handle label first
                                if (aToken.TokenType == TokenTypes.LABEL && aNode.TokenType == TokenTypes.LABEL_REF)
                                {
                                    tmpOpCode = GetLabelValue(aToken.Name);
                                    if (tmpOpCode == 0xffffffff)
                                    {
                                        aToken.ErrorMsg = "Label not found";
                                    }
                                    else
                                    {
                                        tmpOpCode <<= aNode.Position;
                                        if (phaseNo == 2) aLine.OpCode += tmpOpCode;
                                        nextNode = aNode;
                                        break;
                                    }
                                }
                                #endregion --3.2.1 Handle label first

                                #region --3.2.2 Parse token
                                ////else if ((aToken.TokenType == aNode.TokenType) || 
                                ////    (aNode.TokenType == TokenTypes.VALUE4BIT && (aToken.TokenType == TokenTypes.VALUE1BIT)) ||
                                ////    (aNode.TokenType == TokenTypes.VALUE16BIT && (aToken.TokenType == TokenTypes.VALUE4BIT || aToken.TokenType == TokenTypes.VALUE1BIT)) ||
                                ////    (aNode.TokenType == TokenTypes.VALUE17BIT && (aToken.TokenType == TokenTypes.VALUE16BIT || aToken.TokenType == TokenTypes.VALUE4BIT || aToken.TokenType == TokenTypes.VALUE1BIT)))
                                ////{
                                else if ((aToken.TokenType == aNode.TokenType))
                                {
                                    found = true;


                                    #region --Check Do-Loop pair
                                    if (aToken.TokenType == TokenTypes.DO_)
                                    {
                                        if (DoLoopStack.Contains(aToken.Name))
                                            aToken.ErrorMsg = "Duplicat DO commend";
                                        else
                                            DoLoopStack.Add(aToken.Name);
                                    }

                                    if (aToken.TokenType == TokenTypes.LOOP_)
                                    {
                                        if (DoLoopStack.Count == 0)
                                        {
                                            aToken.ErrorMsg = "DO_" + aToken.Name[5] + " Not Found";
                                        }
                                        else if (DoLoopStack[DoLoopStack.Count - 1][3] != aToken.Name[5])
                                        {
                                            aToken.ErrorMsg = "LOOP_" + DoLoopStack[DoLoopStack.Count - 1][3] + " expected";
                                        }
                                        else
                                        {
                                            DoLoopStack.RemoveAt(DoLoopStack.Count - 1);
                                        }
                                    }
                                    #endregion --Check Do-Loop pair

                                    #region --Check Value and Const
                                    if (aNode.TokenType == TokenTypes.VALUE)
                                    {
                                        switch (aNode.Length)
                                        {
                                            case 0: //For SKIPIF TMRxx == 0
                                                if (aToken.Number != 0) aToken.ErrorMsg = "0 expected";
                                                break;
                                            case 1: //1 bit
                                                if (aToken.Number >= 2) aToken.ErrorMsg = "0 - 1 expected";
                                                break;
                                            case 4: //4 bit
                                                if (aToken.Number >= 0x10) aToken.ErrorMsg = "0 - 15 expected";
                                                break;
                                            case 16: //16 bit
                                                if (aToken.Number >= 0x10000) aToken.ErrorMsg = "0 - 0xFFFF (65535) expected";
                                                break;
                                            case 17: //17 bit
                                                if (phaseNo == 2 && aToken.Number > TotalAddr)
                                                {
                                                    aToken.ErrorMsg = "0 - 0x" + TotalAddr.ToString("X5") + "(" + TotalAddr.ToString() + ") expected";
                                                    AddError(aToken);
                                                }
                                                break;
                                            default:
                                                aToken.ErrorMsg = "Parsing error. Please report to I/O Controls";
                                                break;
                                        }
                                    }
                                    else if (aNode.TokenType == TokenTypes.CONST)
                                    {
                                        if (aToken.Number >= 0x20000) aToken.ErrorMsg = "0 - 0x10000 (65536) expected";
                                    }
                                    #endregion --Check Value and const

                                    //Check the length. Such as "AND" is not valid, ANDW or ANDR are valid
                                    else if (aNode.Length != aToken.Name.Length)
                                    {
                                        switch (aNode.TokenType)
                                        {
                                            case TokenTypes.B:
                                                if (aToken.Name.Length != 3 && aToken.Name.Length != 4)
                                                    aToken.ErrorMsg = "B.0 - B.15" + " expected";
                                                break;
                                            case TokenTypes.LABEL:
                                            case TokenTypes.COMMENTS:
                                                break;
                                            default:
                                                aToken.ErrorMsg = "Syntax Error";
                                                break;
                                        }
                                    }

                                    //Handle OP Code
                                    tmpOpCode = aNode.OPCode;
                                    if (aToken.Number != 0xffffffff)
                                    {
                                        tmpOpCode += aToken.Number;
                                    }

                                    tmpOpCode <<= aNode.Position;
                                    if (phaseNo == 2) aLine.OpCode += tmpOpCode;

                                    nextNode = aNode;
                                    break;
                                }
                                #endregion --3.2.2 Parse token

                            }

                            #region --3.2.3 Next token
                            if (found)
                            {
                                currentNode = nextNode;
                            }
                            else
                            {
                                if (phaseNo == 2 && !aLine.HasError)
                                {
                                    aToken.ErrorMsg = "Invalid instruction";
                                    AddError(aToken);
                                    aLine.HasError = true;
                                }
                            }
                            #endregion --3.2.3 Next token

                        }
                        #endregion --3.2 Check if it is valid


                    }
                #endregion --Handle OP Code

                    #region --Handle Incomplete instruction
                    foreach (int aID in currentNode.NodIDs)
                    {
                        SynTreeNode aNode = SyntaxNodeList.Find(aID);
                        if (aNode.TokenType == TokenTypes.END)
                        {
                            complete = true;
                            break;
                        }
                    }
                    if (!complete)
                    {
                        if (phaseNo == 2 && !aLine.HasError)
                        {
                            aLine.TokenList[aLine.TokenList.Count - 1].ErrorMsg = "Incomplete instruction";
                            AddError(aLine.TokenList[aLine.TokenList.Count - 1]);
                            aLine.HasError = true;
                        }
                    }
                    #endregion --Handle Incomplete instruction
                }
            }

            //Check DoLoopStack
            if (DoLoopStack.Count > 0)
                lastToken.ErrorMsg = "LOOP_" + DoLoopStack[DoLoopStack.Count - 1][3] + " expected";
           
        }

        private void AddError(Token token)
        {
            Error err = new Error();
            err.Message = token.ErrorMsg;
            err.LineNo = token.LineNo;
            err.ColNo = token.ColNo;
            err.ProgramLine = ProgramLineList[token.LineNo - 1].Program;
            ErrorList.Add(err);
        }

        private void AddTMR(UInt32 TMRNo)
        {
            bool found = false;
            for (int i = 0; i < TMRList.Count; i++)
            {
                if (TMRList[i] == TMRNo)
                {
                    found = true;
                    break;
                }
                else if (TMRList[i] > TMRNo)
                {
                    TMRList.Insert(i, TMRNo);
                    found = true;
                    break;
                }
            }
            if (!found)
                TMRList.Add(TMRNo);
        }

        private void AddID(UInt32 IDNo)
        {
            bool found = false;
            for (int i = 0; i < IDList.Count; i++)
            {
                if (IDList[i] == IDNo)
                {
                    found = true;
                    break;
                }
                else if (IDList[i] > IDNo)
                {
                    IDList.Insert(i, IDNo);
                    found = true;
                    break;
                }
            }
            if (!found)
                IDList.Add(IDNo);
        }


        private uint GetLabelValue(string LabelName)
        {
            foreach (var aLabel in LabelList)
                if (aLabel.Name == LabelName)
                    return aLabel.Value;
            return 0xFFFFFFFF;
        }

        private bool CheckDuplicateLabel(string LabelName)
        {
            foreach (var aLebel in LabelList)
            {
                if (aLebel.Name == LabelName) return true;
            }
            return false;
        }

        private void SwapByte(byte b1, byte b2)
        {
            byte tByte = b1;
            b1 = b2;
            b2 = tByte;
        }
    }

    public class ExecTime
    {
        public string Name = "";
        public DateTime Time = DateTime.Now;
        public TimeSpan Timespan = new TimeSpan();
    }
}
