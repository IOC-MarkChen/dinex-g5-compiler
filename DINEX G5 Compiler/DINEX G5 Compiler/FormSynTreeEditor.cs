﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using IOControls.DINEX.G5;

namespace G5_Compiler
{
    public partial class FormSynTreeEditor : Form
    {

        SynTreeNode currentNode;
        bool Dirty = false;

        int MaxID = 0;

        public FormSynTreeEditor()
        {
            InitializeComponent();
        }

        private void FormSynTreeEditor_Load(object sender, EventArgs e)
        {
            if (SyntaxNodeList.Items.Count == 0)
            {
                string fileName = Application.StartupPath + "\\SyntaxNodeList1.xml";
                SyntaxNodeList.Load(fileName);
            }            

            RefreshAllNodeDGV();
            Dirty = false;
        }

        private void FormSynTreeEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Dirty)
            {
                switch (MessageBox.Show("Save before closing?", "Confirm", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case System.Windows.Forms.DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                    case System.Windows.Forms.DialogResult.No:
                        break;
                    case System.Windows.Forms.DialogResult.Yes:
                        btnSave_Click(null, null);
                        break;
                }
            }
        }

        private void RefreshAllNodeDGV()
        {
            dgvAllNode.Rows.Clear();
            foreach (SynTreeNode aNode in SyntaxNodeList.Items)
            {
                if (aNode.ID > MaxID) MaxID = aNode.ID;
                AddRowDgvAllNode(aNode);                
            }
        }

        private void btnLoadTree_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            try
            {
                if (!string.IsNullOrEmpty(Properties.Settings.Default.SyntaxNodeFileName))
                {
                    ofd.InitialDirectory = Path.GetDirectoryName(Properties.Settings.Default.SyntaxNodeFileName);
                    ofd.FileName = Properties.Settings.Default.SyntaxNodeFileName;
                }
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Properties.Settings.Default.SyntaxNodeFileName = ofd.FileName;
                    Properties.Settings.Default.Save();

                    SyntaxNodeList.Load(ofd.FileName);
                  
                    RefreshAllNodeDGV();
                    MessageBox.Show("File Loaded");
                    Dirty = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnAddNode_Click(object sender, EventArgs e)
        {
            FormSynTreeNode frm = new FormSynTreeNode();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (frm.TreeNode != null)
                {
                    Dirty = true;
                    MaxID++;
                    frm.TreeNode.ID = MaxID;
                    SyntaxNodeList.Items.Add(frm.TreeNode);
                    AddRowDgvAllNode(frm.TreeNode);                    
                }
            }
        }

        private void dgvAllNode_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            //Show Children node
            dgvChildNode.Rows.Clear();
            txtIDChild.Text = "";
            currentNode = (SynTreeNode)dgvAllNode.Rows[e.RowIndex].Tag; 
            SynTreeNode treeNode;
            foreach (int aID in currentNode.NodIDs)
            {
                treeNode = SyntaxNodeList.Find(aID);
                if (treeNode != null)
                    AddRowDgvChild(treeNode);
            }
        }

        private void AddRowDgvChild(SynTreeNode treeNode)
        {
            
            int index = dgvChildNode.Rows.Add();
            dgvChildNode.Rows[index].Tag = treeNode;
            DataGridViewCellCollection cells = dgvChildNode.Rows[index].Cells;
            cells["ColID2"].Value = treeNode.ID;
            cells["ColName2"].Value = treeNode.NodeName;
            cells["ColNumber2"].Value = treeNode.Number;
            cells["ColType2"].Value = treeNode.TokenType.ToString();
            cells["ColNodeCount2"].Value = treeNode.NodIDs.Count.ToString();
        }

        private void AddRowDgvAllNode(SynTreeNode treeNode)
        {
            Dirty = true;
            int index = dgvAllNode.Rows.Add();
            dgvAllNode.Rows[index].Tag = treeNode;
            DataGridViewCellCollection cells = dgvAllNode.Rows[index].Cells;
            cells["ColID1"].Value = treeNode.ID;
            cells["ColName1"].Value = treeNode.NodeName;
            cells["ColLength1"].Value = treeNode.Length;
            cells["ColNumber1"].Value = treeNode.Number;
            cells["ColType1"].Value = treeNode.TokenType.ToString();
            cells["ColNodeCount1"].Value = treeNode.NodIDs.Count.ToString();
            cells["ColPosition1"].Value = treeNode.Position.ToString();
            cells["ColOPCode1"].Value = treeNode.OPCode.ToString("X8");
        }

        private void UpdateRowDgvAllNode(int RowIndex, SynTreeNode treeNode)
        {
            Dirty = true;
            dgvAllNode.Rows[RowIndex].Tag = treeNode;
            DataGridViewCellCollection cells = dgvAllNode.Rows[RowIndex].Cells;

            cells["ColID1"].Value = treeNode.ID;
            cells["ColName1"].Value = treeNode.NodeName;
            cells["ColNumber1"].Value = treeNode.Number;
            cells["ColType1"].Value = treeNode.TokenType.ToString();
            cells["ColNodeCount1"].Value = treeNode.Nodes.Count.ToString();
            cells["ColPosition1"].Value = treeNode.Position.ToString();
            cells["ColOPCode1"].Value = treeNode.OPCode.ToString("X8");
        }

        private void dgvAllNode_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Edit Node
            FormSynTreeNode frm = new FormSynTreeNode();
            currentNode = (SynTreeNode)dgvAllNode.Rows[e.RowIndex].Tag;
            frm.TreeNode = currentNode;
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)            
                UpdateRowDgvAllNode(e.RowIndex, frm.TreeNode);            
        }


        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (!string.IsNullOrEmpty(Properties.Settings.Default.SyntaxTreeFileName))
                sfd.InitialDirectory = Path.GetDirectoryName(Properties.Settings.Default.SyntaxTreeFileName);
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Properties.Settings.Default.SyntaxNodeFileName = sfd.FileName;
                Properties.Settings.Default.Save();

                SyntaxNodeList.Save(sfd.FileName);
                //synTree.Save(sfd.FileName);

                MessageBox.Show("File Saved");
                Dirty = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SyntaxNodeList.Save(Application.StartupPath + "\\SyntaxNodeList1.xml");             
            MessageBox.Show("File Saved");
            Dirty = false;
        }

        private void btnDelNode_Click(object sender, EventArgs e)
        {
            if (dgvAllNode.SelectedRows.Count == 0) return;

            Dirty = true;
            SynTreeNode delNode = (SynTreeNode)dgvAllNode.SelectedRows[0].Tag;
            int idToDel = delNode.ID;

            foreach (var aNode in SyntaxNodeList.Items)
                aNode.NodIDs.Remove(idToDel);             

            dgvAllNode.Rows.Remove(dgvAllNode.SelectedRows[0]);
            SyntaxNodeList.Items.Remove(delNode);

        }

        private void dgvChildNode_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Drill 
            SynTreeNode parentNode = (SynTreeNode)dgvChildNode.Rows[e.RowIndex].Tag;

            foreach (DataGridViewRow aRow in dgvAllNode.Rows)
            {
                if (parentNode.ID == (int)aRow.Cells["ColID1"].Value)
                {
                    dgvAllNode.ClearSelection();
                    aRow.Selected = true;
                    DataGridViewCellEventArgs eArg = new DataGridViewCellEventArgs(0, dgvAllNode.Rows.IndexOf(aRow));
                    dgvAllNode_CellClick(null, eArg);
                }
            }
        }

        private void btnAddChild_Click(object sender, EventArgs e)
        {
            int idToAdd = 0;
            try
            {
                if (currentNode == null)
                {
                    MessageBox.Show("No node selected");
                    return;

                }
                if (!int.TryParse(txtIDChild.Text, out idToAdd))
                {
                    MessageBox.Show("Invalid ID");
                    return;
                }

                //Find duplicate
                foreach (DataGridViewRow aRow in dgvChildNode.Rows)
                {
                    if (idToAdd == (int)aRow.Cells["ColID2"].Value)
                    {
                        MessageBox.Show("Duplicte child");
                        return;
                    }
                }

                //Add child node

                SynTreeNode childNode = SyntaxNodeList.Find(idToAdd);
                if (childNode == null)
                    throw new Exception("Invalid Node ID");

                currentNode.NodIDs.Add(idToAdd);

                AddRowDgvChild(childNode);
                txtIDChild.Text = "";
                Dirty = true;               
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelChild_Click(object sender, EventArgs e)
        {
            if (dgvChildNode.SelectedRows.Count == 0) return;
            int idToDel = (int)dgvChildNode.SelectedRows[0].Cells["ColID2"].Value;

            currentNode.NodIDs.Remove(idToDel);
            dgvChildNode.Rows.Remove(dgvChildNode.SelectedRows[0]);
            Dirty = true;
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            if (currentNode == null)
            {
                MessageBox.Show("No node selected");
                return;
            }

            int index = dgvAllNode.Rows.IndexOf(dgvAllNode.SelectedRows[0]);
            if (index == 0) return;
            
            //Modify the DGV
            DataGridViewRow row = dgvAllNode.Rows[index];
            dgvAllNode.Rows.RemoveAt(index);
            dgvAllNode.Rows.Insert(index - 1, row);
            dgvAllNode.ClearSelection();
            row.Selected = true;

            //Modify the SyntaxNodeList.Items
            SynTreeNode node = SyntaxNodeList.Items[index];
            SyntaxNodeList.Items.RemoveAt(index);
            SyntaxNodeList.Items.Insert(index - 1, node);
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            if (currentNode == null)
            {
                MessageBox.Show("No node selected");
                return;
            }

            int index = dgvAllNode.Rows.IndexOf(dgvAllNode.SelectedRows[0]);
            if (index == dgvAllNode.Rows.Count - 1) return;

            //Modify the DGV
            DataGridViewRow row = dgvAllNode.Rows[index];
            dgvAllNode.Rows.RemoveAt(index);
            dgvAllNode.Rows.Insert(index + 1, row);
            dgvAllNode.ClearSelection();
            row.Selected = true;

            //Modify the SyntaxNodeList.Items
            SynTreeNode node = SyntaxNodeList.Items[index];
            SyntaxNodeList.Items.RemoveAt(index);
            SyntaxNodeList.Items.Insert(index + 1, node);
        }

        private void btnCopyNode_Click(object sender, EventArgs e)
        {
            SynTreeNode node = currentNode.CopyData(false);
            Dirty = true;
            MaxID++;
            node.ID = MaxID;
            SyntaxNodeList.Items.Add(node);
            AddRowDgvAllNode(node);                    

        }

        private void btnFindParent_Click(object sender, EventArgs e)
        {
            int nodeID = currentNode.ID;

            txtMessage.AppendText("\r\n==Find Parent of ID: " + nodeID.ToString() + " ======\r\n");
            foreach (var aNode in SyntaxNodeList.Items)
            {
                foreach(int childID in aNode.NodIDs)
                    if (childID == nodeID)
                    {
                        txtMessage.AppendText(aNode.ID.ToString() + "\r\n");
                    }
            }
            txtMessage.AppendText("====================================\r\n");

        }

    }
}
