﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using IOControls.DINEX.G5;

namespace G5_Compiler
{
    public partial class FormSynTreeNode : Form
    {
        public SynTreeNode TreeNode = null;
        public FormSynTreeNode()
        {
            InitializeComponent();
        }

        private void FormSynTreeNode_Load(object sender, EventArgs e)
        {
            cbTokenType.DataSource = Enum.GetNames(typeof(TokenTypes));

            if (TreeNode == null) TreeNode = new SynTreeNode();

            txtID.Text = TreeNode.ID.ToString();
            txtLength.Text = TreeNode.Length.ToString();
            txtNodeName.Text = TreeNode.NodeName;
            txtNumber.Text = TreeNode.Number.ToString();
            txtOpCode.Text = TreeNode.OPCode.ToString("X8");
            txtPositioin.Text = TreeNode.Position.ToString();
            cbTokenType.Text = TreeNode.TokenType.ToString();
            cmbDesc.Text = TreeNode.Description;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            TreeNode.ID = int.Parse(txtID.Text);
            TreeNode.Length = int.Parse(txtLength.Text);
            TreeNode.NodeName = txtNodeName.Text;
            TreeNode.Number = int.Parse(txtNumber.Text);
            TreeNode.OPCode = UInt32.Parse(txtOpCode.Text, System.Globalization.NumberStyles.HexNumber);
            TreeNode.Position = int.Parse(txtPositioin.Text);
            TreeNode.TokenType = (TokenTypes)Enum.Parse(typeof(TokenTypes), cbTokenType.Text);
            TreeNode.Description = cmbDesc.Text;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
