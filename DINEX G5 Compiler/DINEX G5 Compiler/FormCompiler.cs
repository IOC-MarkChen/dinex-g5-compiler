﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using IOControls.DINEX.G5;

namespace G5_Compiler
{
    public partial class FormCompiler : Form
    {

        public FormCompiler()
        {
            InitializeComponent();
        }

        private void FormCompiler_Load(object sender, EventArgs e)
        {
            this.Text += " V " + Application.ProductVersion;            

            this.KeyPreview = true;

            string fileName = Application.StartupPath + "\\SyntaxNodeList1.xml";
            if (!File.Exists(fileName))
            {
                MessageBox.Show("Syntax Definition not found!\r\n\r\nPlease re-install the compiler program", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            SyntaxNodeList.Load(fileName);

        }

        private void AssignPathTextBox(TextBox tBox, string messge)
        {
            try
            {
                tBox.Tag = messge;
                int len = tBox.Size.Width / 6;
                string txt = "";
                string dirName, restDirName;
                dirName = Path.GetFileName(messge);
                restDirName = Path.GetDirectoryName(messge);
                while ((txt.Length + dirName.Length) < (len - 7) && restDirName != null)
                {
                    txt = dirName + "\\" + txt;
                    dirName = Path.GetFileName(restDirName);
                    restDirName = Path.GetDirectoryName(restDirName);
                }

                if (restDirName == null)
                    txt = Path.GetPathRoot(messge) + "\\" + txt;
                else
                {
                    txt = Path.GetPathRoot(messge) + "...\\" + txt;
                }
                tBox.Text = txt;
                toolTip1.SetToolTip(tBox, tBox.Tag.ToString());
            }
            catch { }

        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "SRC file|*.src";
            ofd.InitialDirectory = Properties.Settings.Default.FolderName;

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Properties.Settings.Default.FolderName = Path.GetDirectoryName(ofd.FileName);
                Properties.Settings.Default.Save();

                AssignPathTextBox(txtFileName, ofd.FileName);
                txtFileName.Text = Path.GetFileName(ofd.FileName);
                txtMessage.Text = "";
                btnCompile.Enabled = true;

                txtVersion.Text = DateTime.Now.ToString("yyyyMMdd") + "01";
                txtVersion.Enabled = true;
            }
        }

        private void btnCompile_Click(object sender, EventArgs e)
        {
            DINEXG5Compiler compiler = new DINEXG5Compiler();  

            txtMessage.Text = "";
            //Convert Version
            UInt64 version = 0;

            if (!UInt64.TryParse(txtVersion.Text, System.Globalization.NumberStyles.HexNumber, null, out version))
            {
                MessageBox.Show("Invalid Version Number");
                return;
            }

            compiler.Version = BitConverter.GetBytes(version);


            DateTime startTime = DateTime.Now;
            string fileName = txtFileName.Tag.ToString();

            ////if (compiler.Compile(fileName))
            ////{

            ////    txtMessage.AppendText("Total Label Defined: " + compiler.LabelList.Count.ToString() + "\r\n\r\n");
            ////    for (int i = 0; i < compiler.LabelList.Count; i++)
            ////    {
            ////        txtMessage.AppendText(compiler.LabelList[i].Name + ": 0x" + compiler.LabelList[i].Value.ToString("X8") + "\r\n");
            ////    }
            ////    txtMessage.AppendText("=========================\r\n\r\n");

            ////    txtMessage.AppendText("Total Module ID: " + compiler.IDList.Count.ToString() + "\r\n");
            ////    foreach (var aItem in compiler.IDList)                
            ////        txtMessage.AppendText((aItem + 64).ToString("00")  + ", ");
            ////    txtMessage.AppendText("\r\n");
            ////    txtMessage.AppendText("=========================\r\n\r\n");  
              
            ////    txtMessage.AppendText("Total Timer: " + compiler.TMRList.Count.ToString() + "\r\n");
            ////    foreach (var aItem in compiler.TMRList)
            ////        txtMessage.AppendText(aItem.ToString("X2") + ", ");
            ////    txtMessage.AppendText("\r\n");
            ////    txtMessage.AppendText("=========================\r\n\r\n");               
            ////    txtMessage.AppendText("OBJ File Checksum: " + compiler.ChecksumObjFile.ToString("X4") + "\r\n");
            ////    txtMessage.AppendText("Instruction Checksum: " + compiler.ChecksumInstructions.ToString("X4") + "\r\n");

                
            //// }
            ////else
            ////{
            ////    txtMessage.AppendText("Total Error: " + compiler.ErrorList.Count.ToString() + "\r\n\r\n");
            ////    foreach (Error aError in compiler.ErrorList)
            ////    {
            ////        txtMessage.AppendText("Line No: " + aError.LineNo.ToString("00000") + ": " + aError.ProgramLine + " (" + aError.Message + ")\r\n");
            ////    }
            ////}

            compiler.Compile(fileName);
            string[] logFile = File.ReadAllLines(Path.GetDirectoryName(fileName) + "\\" + Path.GetFileNameWithoutExtension(fileName) + ".LOG");
            txtMessage.Lines = logFile;

            TimeSpan exTime = DateTime.Now - startTime;
            txtMessage.AppendText("\r\n\r\nCompile time: " + exTime.TotalSeconds.ToString("0.00") + " seconds\r\n"); 

            if (btnEditTree.Visible)
            {
                txtMessage.AppendText("\r\n");
                for (int i = 0; i < compiler.ExecTimeList.Count; i++)
                {
                    txtMessage.AppendText(compiler.ExecTimeList[i].Name + ": " + compiler.ExecTimeList[i].Time.ToString("HH:mm:ss"));
                    if (i > 0)
                    {
                        compiler.ExecTimeList[i].Timespan = compiler.ExecTimeList[i].Time - compiler.ExecTimeList[i - 1].Time;
                        txtMessage.AppendText("  " + compiler.ExecTimeList[i].Timespan.TotalSeconds.ToString("0.00"));
                    }
                    txtMessage.AppendText("\r\n");
                }
            }
        }


        private void btnEditTree_Click(object sender, EventArgs e)
        {
            FormSynTreeEditor frm = new FormSynTreeEditor();
            frm.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label4_DoubleClick(object sender, EventArgs e)
        {
            btnEditTree.Visible = !btnEditTree.Visible;

        }

        private void FormCompiler_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Shift && e.Alt && e.Control && e.KeyCode == Keys.E)
            {
                btnEditTree.Visible = !btnEditTree.Visible;
            }
        }

    }
}
