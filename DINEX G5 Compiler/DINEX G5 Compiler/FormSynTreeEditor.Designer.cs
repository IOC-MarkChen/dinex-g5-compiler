﻿namespace G5_Compiler
{
    partial class FormSynTreeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAllNode = new System.Windows.Forms.DataGridView();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.btnLoadTree = new System.Windows.Forms.Button();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.btnAddNode = new System.Windows.Forms.Button();
            this.btnAddChild = new System.Windows.Forms.Button();
            this.btnDelChild = new System.Windows.Forms.Button();
            this.btnDelNode = new System.Windows.Forms.Button();
            this.btnCopyNode = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dgvChildNode = new System.Windows.Forms.DataGridView();
            this.ColID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNumber2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColType2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNodeCount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.txtIDChild = new System.Windows.Forms.TextBox();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.btnFindParent = new System.Windows.Forms.Button();
            this.ColID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColLength1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNumber1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColOPCode1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPosition1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColType1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNodeCount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllNode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChildNode)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAllNode
            // 
            this.dgvAllNode.AllowUserToAddRows = false;
            this.dgvAllNode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllNode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColID1,
            this.ColName1,
            this.ColLength1,
            this.ColNumber1,
            this.ColOPCode1,
            this.ColPosition1,
            this.ColType1,
            this.ColNodeCount1});
            this.dgvAllNode.Location = new System.Drawing.Point(27, 87);
            this.dgvAllNode.Name = "dgvAllNode";
            this.dgvAllNode.ReadOnly = true;
            this.dgvAllNode.RowHeadersVisible = false;
            this.dgvAllNode.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAllNode.Size = new System.Drawing.Size(535, 520);
            this.dgvAllNode.TabIndex = 0;
            this.dgvAllNode.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAllNode_CellClick);
            this.dgvAllNode.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAllNode_CellDoubleClick);
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(279, 12);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(75, 23);
            this.btnSaveAs.TabIndex = 11;
            this.btnSaveAs.Text = "Save As";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // btnLoadTree
            // 
            this.btnLoadTree.Location = new System.Drawing.Point(27, 12);
            this.btnLoadTree.Name = "btnLoadTree";
            this.btnLoadTree.Size = new System.Drawing.Size(75, 23);
            this.btnLoadTree.TabIndex = 10;
            this.btnLoadTree.Text = "Load Tree";
            this.btnLoadTree.UseVisualStyleBackColor = true;
            this.btnLoadTree.Click += new System.EventHandler(this.btnLoadTree_Click);
            // 
            // txtMessage
            // 
            this.txtMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMessage.Location = new System.Drawing.Point(568, 299);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMessage.Size = new System.Drawing.Size(327, 309);
            this.txtMessage.TabIndex = 13;
            // 
            // btnAddNode
            // 
            this.btnAddNode.Location = new System.Drawing.Point(27, 58);
            this.btnAddNode.Name = "btnAddNode";
            this.btnAddNode.Size = new System.Drawing.Size(75, 23);
            this.btnAddNode.TabIndex = 14;
            this.btnAddNode.Text = "Add Node";
            this.btnAddNode.UseVisualStyleBackColor = true;
            this.btnAddNode.Click += new System.EventHandler(this.btnAddNode_Click);
            // 
            // btnAddChild
            // 
            this.btnAddChild.Location = new System.Drawing.Point(698, 56);
            this.btnAddChild.Name = "btnAddChild";
            this.btnAddChild.Size = new System.Drawing.Size(75, 23);
            this.btnAddChild.TabIndex = 17;
            this.btnAddChild.Text = "Add Child";
            this.btnAddChild.UseVisualStyleBackColor = true;
            this.btnAddChild.Click += new System.EventHandler(this.btnAddChild_Click);
            // 
            // btnDelChild
            // 
            this.btnDelChild.Location = new System.Drawing.Point(820, 58);
            this.btnDelChild.Name = "btnDelChild";
            this.btnDelChild.Size = new System.Drawing.Size(75, 23);
            this.btnDelChild.TabIndex = 18;
            this.btnDelChild.Text = "Del Child";
            this.btnDelChild.UseVisualStyleBackColor = true;
            this.btnDelChild.Click += new System.EventHandler(this.btnDelChild_Click);
            // 
            // btnDelNode
            // 
            this.btnDelNode.Location = new System.Drawing.Point(108, 58);
            this.btnDelNode.Name = "btnDelNode";
            this.btnDelNode.Size = new System.Drawing.Size(75, 23);
            this.btnDelNode.TabIndex = 19;
            this.btnDelNode.Text = "Del Node";
            this.btnDelNode.UseVisualStyleBackColor = true;
            this.btnDelNode.Click += new System.EventHandler(this.btnDelNode_Click);
            // 
            // btnCopyNode
            // 
            this.btnCopyNode.Location = new System.Drawing.Point(189, 58);
            this.btnCopyNode.Name = "btnCopyNode";
            this.btnCopyNode.Size = new System.Drawing.Size(75, 23);
            this.btnCopyNode.TabIndex = 20;
            this.btnCopyNode.Text = "Copy Node";
            this.btnCopyNode.UseVisualStyleBackColor = true;
            this.btnCopyNode.Click += new System.EventHandler(this.btnCopyNode_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(198, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 21;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgvChildNode
            // 
            this.dgvChildNode.AllowUserToAddRows = false;
            this.dgvChildNode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChildNode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColID2,
            this.ColName2,
            this.ColNumber2,
            this.ColType2,
            this.ColNodeCount2});
            this.dgvChildNode.Location = new System.Drawing.Point(568, 87);
            this.dgvChildNode.MultiSelect = false;
            this.dgvChildNode.Name = "dgvChildNode";
            this.dgvChildNode.ReadOnly = true;
            this.dgvChildNode.RowHeadersVisible = false;
            this.dgvChildNode.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvChildNode.Size = new System.Drawing.Size(327, 206);
            this.dgvChildNode.TabIndex = 22;
            this.dgvChildNode.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvChildNode_CellDoubleClick);
            // 
            // ColID2
            // 
            this.ColID2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColID2.FillWeight = 50F;
            this.ColID2.HeaderText = "ID";
            this.ColID2.Name = "ColID2";
            this.ColID2.ReadOnly = true;
            // 
            // ColName2
            // 
            this.ColName2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColName2.HeaderText = "Name";
            this.ColName2.Name = "ColName2";
            this.ColName2.ReadOnly = true;
            // 
            // ColNumber2
            // 
            this.ColNumber2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColNumber2.FillWeight = 50F;
            this.ColNumber2.HeaderText = "Number";
            this.ColNumber2.Name = "ColNumber2";
            this.ColNumber2.ReadOnly = true;
            // 
            // ColType2
            // 
            this.ColType2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColType2.HeaderText = "Type";
            this.ColType2.Name = "ColType2";
            this.ColType2.ReadOnly = true;
            // 
            // ColNodeCount2
            // 
            this.ColNodeCount2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColNodeCount2.FillWeight = 50F;
            this.ColNodeCount2.HeaderText = "Node Count";
            this.ColNodeCount2.Name = "ColNodeCount2";
            this.ColNodeCount2.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(565, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "ID:";
            // 
            // txtIDChild
            // 
            this.txtIDChild.Location = new System.Drawing.Point(592, 58);
            this.txtIDChild.Name = "txtIDChild";
            this.txtIDChild.Size = new System.Drawing.Size(100, 20);
            this.txtIDChild.TabIndex = 26;
            this.txtIDChild.Text = "0";
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Location = new System.Drawing.Point(484, 58);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(75, 23);
            this.btnMoveDown.TabIndex = 27;
            this.btnMoveDown.Text = "MoveDown";
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Location = new System.Drawing.Point(403, 58);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(75, 23);
            this.btnMoveUp.TabIndex = 28;
            this.btnMoveUp.Text = "Move Up";
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnFindParent
            // 
            this.btnFindParent.Location = new System.Drawing.Point(322, 58);
            this.btnFindParent.Name = "btnFindParent";
            this.btnFindParent.Size = new System.Drawing.Size(75, 23);
            this.btnFindParent.TabIndex = 29;
            this.btnFindParent.Text = "Find Parent";
            this.btnFindParent.UseVisualStyleBackColor = true;
            this.btnFindParent.Click += new System.EventHandler(this.btnFindParent_Click);
            // 
            // ColID1
            // 
            this.ColID1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColID1.FillWeight = 50F;
            this.ColID1.HeaderText = "ID";
            this.ColID1.Name = "ColID1";
            this.ColID1.ReadOnly = true;
            // 
            // ColName1
            // 
            this.ColName1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColName1.HeaderText = "Name";
            this.ColName1.Name = "ColName1";
            this.ColName1.ReadOnly = true;
            // 
            // ColLength1
            // 
            this.ColLength1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColLength1.FillWeight = 50F;
            this.ColLength1.HeaderText = "Len";
            this.ColLength1.Name = "ColLength1";
            this.ColLength1.ReadOnly = true;
            // 
            // ColNumber1
            // 
            this.ColNumber1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColNumber1.FillWeight = 50F;
            this.ColNumber1.HeaderText = "Number";
            this.ColNumber1.Name = "ColNumber1";
            this.ColNumber1.ReadOnly = true;
            // 
            // ColOPCode1
            // 
            this.ColOPCode1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColOPCode1.HeaderText = "OP Code";
            this.ColOPCode1.Name = "ColOPCode1";
            this.ColOPCode1.ReadOnly = true;
            // 
            // ColPosition1
            // 
            this.ColPosition1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColPosition1.FillWeight = 50F;
            this.ColPosition1.HeaderText = "Position";
            this.ColPosition1.Name = "ColPosition1";
            this.ColPosition1.ReadOnly = true;
            // 
            // ColType1
            // 
            this.ColType1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColType1.HeaderText = "Type";
            this.ColType1.Name = "ColType1";
            this.ColType1.ReadOnly = true;
            // 
            // ColNodeCount1
            // 
            this.ColNodeCount1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColNodeCount1.FillWeight = 50F;
            this.ColNodeCount1.HeaderText = "Node Count";
            this.ColNodeCount1.Name = "ColNodeCount1";
            this.ColNodeCount1.ReadOnly = true;
            // 
            // FormSynTreeEditor
            // 
            this.AcceptButton = this.btnAddChild;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 619);
            this.Controls.Add(this.btnFindParent);
            this.Controls.Add(this.btnMoveUp);
            this.Controls.Add(this.btnMoveDown);
            this.Controls.Add(this.txtIDChild);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvChildNode);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCopyNode);
            this.Controls.Add(this.btnDelNode);
            this.Controls.Add(this.btnDelChild);
            this.Controls.Add(this.btnAddChild);
            this.Controls.Add(this.btnAddNode);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.btnLoadTree);
            this.Controls.Add(this.dgvAllNode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSynTreeEditor";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormSynTreeEditor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSynTreeEditor_FormClosing);
            this.Load += new System.EventHandler(this.FormSynTreeEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllNode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChildNode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAllNode;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.Button btnLoadTree;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Button btnAddNode;
        private System.Windows.Forms.Button btnAddChild;
        private System.Windows.Forms.Button btnDelChild;
        private System.Windows.Forms.Button btnDelNode;
        private System.Windows.Forms.Button btnCopyNode;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dgvChildNode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNumber2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColType2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNodeCount2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIDChild;
        private System.Windows.Forms.Button btnMoveDown;
        private System.Windows.Forms.Button btnMoveUp;
        private System.Windows.Forms.Button btnFindParent;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLength1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNumber1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColOPCode1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPosition1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColType1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNodeCount1;
    }
}